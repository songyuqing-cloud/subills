<?php

namespace App\Http\Controllers;

use App\Models\AfinTipo;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class AfinTiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $afintipos = AfinTipo::all();
        $data = [
            'afintipos' => $afintipos
          ];
          return view('afintipos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $afintipo = new AfinTipo();  
        $afintipo ->nombre = '';
        $data = [
            'afintipo' => $afintipo,
            'create' => true,
        ];
        return view('afintipos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     */
    public function store(Request $request)
    {
        $afinTipo = new AfinTipo();
        $afinTipo->nombre = $request->nombre;

        try {
            $afinTipo->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $afinTipo = AfinTipo::find($id);
        $data = [
            'afintipo' => $afinTipo
        ];
        return view('afintipos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AfinTipo  $afinTipo 
     */
    public function edit($id)
    {
        $afintipo = AfinTipo::find($id);
        $data = [
            'afintipo' => $afintipo,
            'create' => 'false',
        ];
        return view('afintipos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AfinTipo  $afinTipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, )
    {
        $afinTipo = AfinTipo::find($request->id);
        $afinTipo->nombre = $request->nombre;

        try {
            $afinTipo->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AfinTipo  $afinTipo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $afinTipo = AfinTipo::find($id);
        try {
            $afinTipo->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data; 
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Beneficiario;
use App\Models\Fondo;
use App\Models\Incidencia;
use App\Models\Proyecto;
use App\Models\Solicitud;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;

class SolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *

     */
    public function index()
    {
        $solicitudes = Solicitud::with(['status', 'proyecto', 'beneficiario', 'fondo', 'incidencia', 'user'])->get();
        $data = [
            'solicitudes' => $solicitudes
        ];
        return view('solicitudes.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *

     */
    public function create()
    {
        $solicitud = new Solicitud();
        $users = User::all();
        $status = Status::all();
        $proyectos = Proyecto::all();
        $fondos = Fondo::all();
        $beneficiarios = Beneficiario::all();
        $incidencias = Incidencia::all();
        $solicitud ->id_status = 0;
        $solicitud ->id_proyecto = 0;
        $solicitud ->id_beneficiario = 0;
        $solicitud ->id_fondo = 0;
        $solicitud ->id_incidencia = 0;
        $solicitud ->id_usuario_solicitante = 0;
        $solicitud ->fecha = null;
        $solicitud ->fecha_pago= null;
        $solicitud ->folio = '';
        $solicitud ->concepto = '';
        $solicitud ->tipo = '';
        $solicitud ->tipo_afin = '';
        $solicitud ->monto = 0;
        $solicitud ->reintegro = 0;
        $solicitud ->solicitud_afin = 0;
        $solicitud ->solicitud_afin = 0;
        $solicitud ->aprobacion_afin = 0;
        $solicitud ->orden_compra = 0;
        $solicitud ->orden_compra_afin = 0;
        $solicitud ->recepcion_afin = 0;
        $solicitud ->factura_afin = 0;
        $solicitud ->movimiento = '';
        $solicitud ->paquete_finanzas = 0;
        $solicitud ->recibido_finanzas = null;
        $solicitud ->observaciones = '';

        $data = [
            'solicitud' => $solicitud,
            'users' => $users,
            'status' => $status,
            'proyectos' => $proyectos,
            'fondos' => $fondos,
            'beneficiarios' => $beneficiarios,
            'incidencias' => $incidencias,
            'create' => true,
        ];
        return view('solicitudes.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $solicitud = new Solicitud();
        $solicitud ->id_status = $request-> id_status;
        $solicitud ->id_proyecto = $request ->id_proyecto;
        $solicitud ->id_beneficiario = $request ->id_beneficiario;
        $solicitud ->id_fondo = $request ->id_fondo;
        $solicitud ->id_incidencia = $request ->id_incidencia;
        $solicitud ->id_usuario_solicitante = $request ->id_usuario_solicitante;
        $solicitud ->fecha =  $request ->fecha;
        $solicitud ->fecha_pago=  $request ->fecha_pago;
        $solicitud ->folio =  $request ->folio;
        $solicitud ->concepto =  $request ->concepto;
        $solicitud ->tipo =  $request ->tipo;
        $solicitud ->tipo_afin =  $request ->tipo_afin;
        $solicitud ->monto = $request ->monto;
        $solicitud ->reintegro = $request ->reintegro;
        $solicitud ->solicitud_afin = $request ->solicitud_afin;
        $solicitud ->solicitud_afin = $request ->solicitud_afin;
        $solicitud ->aprobacion_afin = $request ->aprobacion_afin;
        $solicitud ->orden_compra = $request ->orden_compra;
        $solicitud ->orden_compra_afin = $request ->orden_compra_afin;
        $solicitud ->recepcion_afin = $request ->recepcion_afin;
        $solicitud ->factura_afin = $request ->factura_afin;
        $solicitud ->movimiento =  $request ->movimiento;
        $solicitud ->paquete_finanzas = $request ->paquete_finanzas;
        $solicitud ->recibido_finanzas =  $request ->recibido_finanzas;
        $solicitud ->observaciones =  $request ->observaciones;
        try {
            $solicitud->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Solicitud  $solicitud
     */
    public function show($id)
    {
        $solicitud = Solicitud::with(['status', 'proyecto', 'beneficiario', 'fondo', 'incidencia', 'user'])->find($id);
        $data = [
            'solicitud' => $solicitud
        ];
        return view('solicitudes.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Solicitud  $solicitud
     */
    public function edit($id)
    {
        $users = User::all();
        $status = Status::all();
        $proyectos = Proyecto::all();
        $fondos = Fondo::all();
        $beneficiarios = Beneficiario::all();
        $incidencias = Incidencia::all();
        $solicitud = Solicitud::find($id);
        $data = [
            'solicitud' => $solicitud,
            'users' => $users,
            'status' => $status,
            'proyectos' => $proyectos,
            'fondos' => $fondos,
            'beneficiarios' => $beneficiarios,
            'incidencias' => $incidencias,
            'create' => 'false',
        ];
        return view('solicitudes.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $solicitud = Solicitud::find($request->id);
        $solicitud ->id_status = $request-> id_status;
        $solicitud ->id_proyecto = $request ->id_proyecto;
        $solicitud ->id_beneficiario = $request ->id_beneficiario;
        $solicitud ->id_fondo = $request ->id_fondo;
        $solicitud ->id_incidencia = $request ->id_incidencia;
        $solicitud ->id_usuario_solicitante = $request ->id_usuario_solicitante;
        $solicitud ->fecha =  $request ->fecha;
        $solicitud ->fecha_pago=  $request ->fecha_pago;
        $solicitud ->folio =  $request ->folio;
        $solicitud ->concepto =  $request ->concepto;
        $solicitud ->tipo =  $request ->tipo;
        $solicitud ->tipo_afin =  $request ->tipo_afin;
        $solicitud ->monto = $request ->monto;
        $solicitud ->reintegro = $request ->reintegro;
        $solicitud ->solicitud_afin = $request ->solicitud_afin;
        $solicitud ->solicitud_afin = $request ->solicitud_afin;
        $solicitud ->aprobacion_afin = $request ->aprobacion_afin;
        $solicitud ->orden_compra = $request ->orden_compra;
        $solicitud ->orden_compra_afin = $request ->orden_compra_afin;
        $solicitud ->recepcion_afin = $request ->recepcion_afin;
        $solicitud ->factura_afin = $request ->factura_afin;
        $solicitud ->movimiento =  $request ->movimiento;
        $solicitud ->paquete_finanzas = $request ->paquete_finanzas;
        $solicitud ->recibido_finanzas =  $request ->recibido_finanzas;
        $solicitud ->observaciones =  $request ->observaciones;
        try {
            $solicitud->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $solicitud = Solicitud::find($id);
        try {
            $solicitud->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}

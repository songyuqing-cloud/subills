<?php

namespace App\Http\Controllers;

use App\Models\FacturaArchivo;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class FacturaArchivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $factura_archivos = FacturaArchivo::with(['solicitud'])->get();
        $data = [
            'factura_archivos' => $factura_archivos
        ];
        return view('factura_archivos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $solicitudes = Solicitud::all();
        $factura_archivo = new FacturaArchivo();
        $factura_archivo->id_solicitud = 0;
        $factura_archivo->nombre = '';


        $data = [
            'factura_archivo' => $factura_archivo,
            'solicitudes' => $solicitudes,
            'create' => true,
        ];

        return view('factura_archivos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $factura_archivo = new FacturaArchivo();
        $factura_archivo->id_solicitud = $request-> id_solicitud;
        $factura_archivo->nombre = $request-> nombre;

        try {
            $factura_archivo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
            dd($e);
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $factura_archivo = FacturaArchivo::with('solicitud')->find($id);
        $data = [
            'factura_archivo' => $factura_archivo
        ];
        return view('factura_archivos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $factura_archivo = FacturaArchivo::find($id);
        $solicitudes = Solicitud::all();

        $data = [
            'factura_archivo' => $factura_archivo,
            'solicitudes' => $solicitudes,
            'create' => 'false',
        ];

        return view('factura_archivos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $factura_archivo = FacturaArchivo::find($request->id);
        $factura_archivo->id_solicitud = $request-> id_solicitud;
        $factura_archivo->nombre = $request-> nombre;

        try {
            $factura_archivo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
            dd($e);
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($id)
    {
        $factura_archivo = FacturaArchivo::find($id);
        try {
            $factura_archivo->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}

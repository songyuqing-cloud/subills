<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use Illuminate\Http\Request;

class PaquetesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paquetes = Paquete::all();
        $data = [
          'paquetes' => $paquetes
        ];
        return view('paquetes.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paquete = new Paquete();
        $paquete ->numero = '';
        $paquete ->solicitud_afin = '';
        $data = [
            'paquete' => $paquete,
            'create' => true,
        ];
        return view('paquetes.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paquete = new Paquete();
        $paquete->solicitud_afin = $request -> solicitud_afin;
        $paquete->numero = $request -> numero;
        try {
            $paquete->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paquete = Paquete::find($id);
        $data = [
            'paquete' => $paquete
        ];
        return view('paquetes.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paquete = Paquete::find($id);
        $data = [
            'paquete' => $paquete,
            'create' => 'false',
        ];
        return view('paquetes.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $paquete = Paquete::find($request->id);
        $paquete -> solicitud_afin = $request->solicitud_afin;
        $paquete -> numero = $request->numero;
        try {
            $paquete->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paquete = Paquete::find($id);
        try {
            $paquete->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Movimiento;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class MovimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movimientos = Movimiento::with('solicitude')->get();
        $data = [
            'movimientos' => $movimientos
        ];
        return view('movimientos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $movimiento = new Movimiento();
        $movimiento->fecha = '';
        $movimiento->descripcion = '';
        $movimiento->monto = '';
        $movimiento->tipo = '';
        $movimiento->folio = '';
        $movimiento->id_solicitud = '';
        $solicitudes = Solicitud::all();
        $data = [
            'movimiento' => $movimiento,
            'create' => true,
            'solicitudes' => $solicitudes,
        ];
        return view('movimientos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movimiento = new Movimiento();
        $movimiento->fecha = $request->fecha;
        $movimiento->descripcion = $request->descripcion;
        $movimiento->monto = $request->monto;
        $movimiento->tipo = $request->tipo;
        $movimiento->folio = $request->folio;
        $movimiento->id_solicitud = $request->id_solicitud;
        try {
            $movimiento->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movimiento = Movimiento::with('solicitude')->find($id);
        $data = [
            'movimiento' => $movimiento
        ];
        return view('movimientos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movimiento = Movimiento::find($id);
        $solicitudes = Solicitud::all();
        $data = [
            'movimiento' => $movimiento,
            'create' => 'false',
            'solicitudes' => $solicitudes
        ];
        return view('movimientos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movimiento $movimiento)
    {
        $movimiento = Movimiento::find($request->id);
        $movimiento->fecha = $request->fecha;
        $movimiento->descripcion = $request->descripcion;
        $movimiento->monto = $request->monto;
        $movimiento->tipo = $request->tipo;
        $movimiento->folio = $request->folio;
        $movimiento->id_solicitud = $request->id_solicitud;
        try {
            $movimiento->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movimiento = Movimiento::find($id);
        try {
            $movimiento->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}

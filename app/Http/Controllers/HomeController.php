<?php

namespace App\Http\Controllers;

use App\Models\Solicitud;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $solicitudes = Solicitud::with(['status', 'proyecto', 'beneficiario', 'fondo', 'incidencia', 'user'])->get();
        $data = [
            'solicitudes' => $solicitudes
        ];
//        return view('solicitudes.table', $data);
        return view('home', $data);
    }
}

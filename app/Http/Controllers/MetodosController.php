<?php

namespace App\Http\Controllers;

use App\Models\Metodo;
use Illuminate\Http\Request;

class MetodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metodos = Metodo::all();
        $data = [
            'metodos' => $metodos
          ];
          return view('metodos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $metodo = new Metodo();  
        $metodo ->nombre = '';
        $data = [
            'metodo' => $metodo,
            'create' => true,
        ];
        return view('metodos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $metodo = new Metodo();
        $metodo->nombre = $request->nombre;

        try {
            $metodo->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Metodo  $metodo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $metodo = Metodo::find($id);
        $data = [
            'metodo' => $metodo
        ];
        return view('metodos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Metodo  $metodo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $metodo = Metodo::find($id);
        $data = [
            'metodo' => $metodo,
            'create' => 'false',
        ];
        return view('metodos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Metodo  $metodo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $metodo = Metodo::find($request->id);
        $metodo->nombre = $request->nombre;

        try {
            $metodo->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Metodo  $metodo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $metodo = Metodo::find($id);
        try {
            $metodo->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data; 
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Partida;
use App\Models\Proyecto;
use Illuminate\Http\Request;

class PartidasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partidas = Partida::with('proyecto')->get();
        $data = [
            'partidas' => $partidas
        ];
        return view('partidas.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partida = new Partida();
        $partida->id_proyecto = '';
        $partida->numero = '';
        $partida->monto = '';
        $partida->descripcion = '';
        $partida->cog = '';
        $proyectos = Proyecto::all();
        $data = [
            'partida' => $partida,
            'create' => true,
            'proyectos' => $proyectos,
        ];
        return view('partidas.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $partida = new Partida();
        $partida->id_proyecto = $request->id_proyecto;
        $partida->numero = $request->numero;
        $partida->monto = $request->monto;
        $partida->descripcion = $request->descripcion;
        $partida->cog = $request->cog;
        try {
            $partida->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad:';
            dd($e);
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partida  $partida
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $partida = Partida::with('proyecto')->find($id);
        $data = [
            'partida' => $partida
        ];
        return view('partidas.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partida  $partida
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partida = Partida::find($id);
        $proyectos = Proyecto::all();
        $data = [
            'partida' => $partida,
            'create' => 'false',
            'proyectos' => $proyectos
        ];
        return view('partidas.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Partida  $partida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partida $partida)
    {
        $partida = Partida::find($request->id);
        $partida->id_proyecto = $request->id_proyecto;
        $partida->numero = $request->numero;
        $partida->monto = $request->monto;
        $partida->descripcion = $request->descripcion;
        $partida->cog = $request->cog;
        try {
            $partida->save();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partida  $partida
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partida = Partida::find($id);
        try {
            $partida->delete();
            $information = 'good';
        } catch (\Illuminate\Database\QueryException $e) {
            $information = 'bad';
        }
        $data = ['information' => $information];
        return $data;
    }
}

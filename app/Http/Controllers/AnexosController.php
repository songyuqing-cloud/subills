<?php

namespace App\Http\Controllers;

use App\Models\Anexo;
use App\Models\Solicitud;
use Illuminate\Http\Request;

class AnexosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anexos = Anexo::with('solicitud')->get();
        $data = [
            'anexos' => $anexos
        ];
        return view('anexos.table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anexo = new Anexo();
        $anexo ->nombre = '';
        $anexo ->id_solicitud = '';
        $solicitudes = Solicitud::all();
        $data = [
            'anexo' => $anexo,
            'create' => true,
            'solicitudes' => $solicitudes,
        ];
        return view('anexos.create-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $anexo = new Anexo();
        $anexo->nombre = $request ->nombre;
        $anexo->id_solicitud = $request -> id_solicitud;
        try {
            $anexo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Anexo  $anexo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anexo = Anexo::with('solicitud')->find($id);
        $data = [
            'anexo' => $anexo
        ];
        return view('anexos.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Anexo  $anexo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anexo = Anexo::find($id);
        $solicitudes = Solicitud::all();
        $data = [
            'anexo' => $anexo,
            'create' => 'false',
            'solicitudes' => $solicitudes
        ];
        return view('anexos.create-add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Anexo  $anexo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Anexo $anexo)
    {
        $anexo = Anexo::find($request->id);
        $anexo -> nombre = $request->nombre;
        $anexo -> id_solicitud = $request->id_solicitud;
        try {
            $anexo->save();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Anexo  $anexo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anexo = Anexo::find($id);
        try {
            $anexo->delete();
            $information = 'good';

        }catch (\Illuminate\Database\QueryException $e){
            $information = 'bad';
        }
        $data = ['information'=> $information];
        return $data;
    }
}

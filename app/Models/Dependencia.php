<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Dependencia
 * 
 * @property int $id
 * @property string $nombre
 * @property string $abreviatura
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Beneficiario[] $beneficiarios
 *
 * @package App\Models
 */
class Dependencia extends Model
{
	protected $table = 'dependencias';

	protected $fillable = [
		'nombre',
		'abreviatura'
	];

	public function beneficiarios()
	{
		return $this->hasMany(Beneficiario::class, 'id_dependencia');
	}
}

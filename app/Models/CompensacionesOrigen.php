<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CompensacionesOrigen
 *
 * @property int|null $id_compensacion
 * @property int|null $id_partida
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Compensaciones|null $compensacione
 * @property Partida|null $partida
 *
 * @package App\Models
 */
class CompensacionesOrigen extends Model
{
	protected $table = 'compensaciones_origen';
	public $incrementing = false;

	protected $casts = [
		'id_compensacion' => 'int',
		'id_partida' => 'int'
	];

	protected $fillable = [
		'id_compensacion',
		'id_partida'
	];

	public function compensaciones()
	{
		return $this->belongsTo(Compensaciones::class, 'id_compensacion');
	}

	public function partida()
	{
		return $this->belongsTo(Partida::class, 'id_partida');
	}
}

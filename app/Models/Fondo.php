<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fondo
 *
 * @property int $id
 * @property int|null $numero
 * @property string|null $nombre
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Collection|Bolsa[] $bolsas
 * @property Collection|Proyecto[] $proyectos
 * @property Collection|SaldoProyecto[] $saldo_proyectos
 * @property Collection|SaldoPartida[] $saldos_partidas
 * @property Collection|Solicitud[] $solicitudes
 *
 * @package App\Models
 */
class Fondo extends Model
{
	protected $table = 'fondos';

	protected $casts = [
		'numero' => 'int'
	];

	protected $fillable = [
		'numero',
		'nombre'
	];

	public function bolsas()
	{
		return $this->hasMany(Bolsa::class, 'id_fondo');
	}

	public function proyectos()
	{
		return $this->hasMany(Proyecto::class, 'id_fondo');
	}

	public function saldo_proyectos()
	{
		return $this->hasMany(SaldoProyecto::class, 'id_fondo');
	}

	public function saldos_partidas()
	{
		return $this->hasMany(SaldoPartida::class, 'id_fondo');
	}

	public function solicitudes()
	{
		return $this->hasMany(Solicitud::class, 'id_fondo');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reintegro
 *
 * @property int $id
 * @property int|null $folio
 * @property int|null $id_solicitud
 * @property Carbon|null $fecha
 * @property string|null $concepto
 * @property int|null $referencia
 * @property float|null $monto
 * @property string|null $observaciones
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Solicitud|null $solicitude
 *
 * @package App\Models
 */
class Reintegro extends Model
{
	protected $table = 'reintegros';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'folio' => 'int',
		'id_solicitud' => 'int',
		'referencia' => 'int',
		'monto' => 'float'
	];

	protected $dates = [
		'fecha'
	];

	protected $fillable = [
		'folio',
		'id_solicitud',
		'fecha',
		'concepto',
		'referencia',
		'monto',
		'observaciones'
	];

	public function solicitude()
	{
		return $this->belongsTo(Solicitud::class, 'id_solicitud');
	}
}

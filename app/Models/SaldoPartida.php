<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SaldoPartida
 *
 * @property int $id
 * @property int|null $id_fondo
 * @property int|null $id_partida
 * @property float|null $inicial
 * @property float|null $adeudo
 * @property float|null $capturado
 * @property float|null $solicitado
 * @property float|null $pagado
 * @property float|null $compensado
 * @property float|null $porcentaje_compensado
 * @property float|null $saldo
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Fondo|null $fondo
 * @property Partida|null $partida
 *
 * @package App\Models
 */
class SaldoPartida extends Model
{
    protected $table = 'saldo_partidas';

    protected $casts = [
        'id_fondo' => 'int',
        'id_partida' => 'int',
        'inicial' => 'float',
        'adeudo' => 'float',
        'capturado' => 'float',
        'solicitado' => 'float',
        'pagado' => 'float',
        'compensado' => 'float',
        'porcentaje_compensado' => 'float',
        'saldo' => 'float'
    ];

    protected $fillable = [
        'id_fondo',
        'id_partida',
        'inicial',
        'adeudo',
        'capturado',
        'solicitado',
        'pagado',
        'compensado',
        'porcentaje_compensado',
        'saldo'
    ];

    public function fondo()
    {
        return $this->belongsTo(Fondo::class, 'id_fondo');
    }

    public function partida()
    {
        return $this->belongsTo(Partida::class, 'id_partida');
    }
}

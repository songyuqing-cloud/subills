<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Proveedor
 *
 * @property int $id
 * @property string|null $razon_social
 * @property string|null $rfc
 * @property string|null $cuenta
 * @property string|null $direccion
 * @property string|null $correo
 * @property string|null $telefono
 * @property string|null $observaciones
 * @property int|null $id_banco
 * @property string|null $clabe
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 *
 * @property Banco|null $banco
 *
 * @package App\Models
 */
class Proveedor extends Model
{
	protected $table = 'proveedores';

	protected $casts = [
		'id_banco' => 'int'
	];

	protected $fillable = [
		'razon_social',
		'rfc',
		'cuenta',
		'direccion',
		'correo',
		'telefono',
		'observaciones',
		'id_banco',
		'clabe'
	];

	public function banco()
	{
		return $this->belongsTo(Banco::class, 'id_banco');
	}
}

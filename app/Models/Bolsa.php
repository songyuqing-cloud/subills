<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bolsa
 * 
 * @property int $id
 * @property int|null $numero
 * @property string|null $nombre
 * @property int|null $id_fondo
 * @property string|null $fuente_financiamento
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 * 
 * @property Fondo|null $fondo
 *
 * @package App\Models
 */
class Bolsa extends Model
{
	protected $table = 'bolsas';

	protected $casts = [
		'numero' => 'int',
		'id_fondo' => 'int'
	];

	protected $fillable = [
		'numero',
		'nombre',
		'id_fondo',
		'fuente_financiamento'
	];

	public function fondo()
	{
		return $this->belongsTo(Fondo::class, 'id_fondo');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 *
 * @property int $id
 * @property int|null $role_id
 * @property string $email
 * @property string|null $avatar
 * @property string $password
 * @property string|null $remember_token
 * @property string|null $settings
 * @property string $name
 * @property string $phone
 * @property int|null $id_dependencia
 * @property Carbon|null $email_verified_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Role|null $role
 * @property Collection|Solicitud[] $solicitudes
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class User extends Model
{
	protected $table = 'users';

	protected $casts = [
		'role_id' => 'int',
		'id_dependencia' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'role_id',
		'email',
		'avatar',
		'password',
		'remember_token',
		'settings',
		'name',
		'phone',
		'id_dependencia',
		'email_verified_at'
	];

	public function role()
	{
		return $this->belongsTo(Role::class);
	}

	public function solicitudes()
	{
		return $this->hasMany(Solicitud::class, 'id_usuario_solicitante');
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'user_roles');
	}
}

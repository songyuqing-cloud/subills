<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SaldoProyecto
 * 
 * @property int $id
 * @property int|null $id_fondo
 * @property int|null $id_proyecto
 * @property float|null $inicial
 * @property float|null $capturado
 * @property float|null $solicitado
 * @property float|null $pagado
 * @property float|null $compensado
 * @property float|null $porcentaje_compensado
 * @property float|null $saldo
 * @property float|null $comprobado
 * @property float|null $adeudo
 * @property Carbon|null $updated_at
 * @property Carbon|null $created_at
 * 
 * @property Fondo|null $fondo
 * @property Proyecto|null $proyecto
 *
 * @package App\Models
 */
class SaldoProyecto extends Model
{
	protected $table = 'saldo_proyectos';

	protected $casts = [
		'id_fondo' => 'int',
		'id_proyecto' => 'int',
		'inicial' => 'float',
		'capturado' => 'float',
		'solicitado' => 'float',
		'pagado' => 'float',
		'compensado' => 'float',
		'porcentaje_compensado' => 'float',
		'saldo' => 'float',
		'comprobado' => 'float',
		'adeudo' => 'float'
	];

	protected $fillable = [
		'id_fondo',
		'id_proyecto',
		'inicial',
		'capturado',
		'solicitado',
		'pagado',
		'compensado',
		'porcentaje_compensado',
		'saldo',
		'comprobado',
		'adeudo'
	];

	public function fondo()
	{
		return $this->belongsTo(Fondo::class, 'id_fondo');
	}

	public function proyecto()
	{
		return $this->belongsTo(Proyecto::class, 'id_proyecto');
	}
}

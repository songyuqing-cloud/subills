/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import Vue from 'vue'
import Buefy from 'buefy'

import 'buefy/dist/buefy.css'


Vue.use(Buefy)

Vue.mixin({
  methods: {
    showSuccessNotification(text) {
      this.$buefy.notification.open({
        message: text,
        duration: 3000,
        type: "is-success",
        position: "is-bottom-right",
      });
    },
    showErrorNotification(text) {
      this.$buefy.notification.open({
        message: text,
        duration: 3000,
        type: "is-danger",
        position: "is-bottom-right",
      });
    },
  }
})


Vue.component('side-bar', require('./components/SideBar').default);


Vue.component('detail-fondo-component', require('./components/fondos/DetailComponent.vue').default);
Vue.component('create-edit-fondo-component', require('./components/fondos/CreateEditComponent').default);
Vue.component('table-fondo-component', require('./components/fondos/TableComponent').default);

Vue.component('detail-solicitud-component', require('./components/solicitudes/DetailComponent.vue').default);
Vue.component('create-edit-solicitud-component', require('./components/solicitudes/CreateEditComponent').default);
Vue.component('table-solicitud-component', require('./components/solicitudes/TableComponent').default);

Vue.component('detail-saldoproyectos-component', require('./components/saldo_proyectos/DetailComponent.vue').default);
Vue.component('create-edit-saldoproyectos-component', require('./components/saldo_proyectos/CreateEditComponent').default);
Vue.component('table-saldoproyectos-component', require('./components/saldo_proyectos/TableComponent').default);

Vue.component('detail-saldopartidas-component', require('./components/saldo_partidas/DetailComponent.vue').default);
Vue.component('create-edit-saldopartidas-component', require('./components/saldo_partidas/CreateEditComponent').default);
Vue.component('table-saldopartidas-component', require('./components/saldo_partidas/TableComponent').default);

Vue.component('detail-afintipo-component', require('./components/afintipos/DetailComponent.vue').default);
Vue.component('create-edit-afintipo-component', require('./components/afintipos/CreateEditComponent').default);
Vue.component('table-afintipo-component', require('./components/afintipos/TableComponent').default);

Vue.component('detail-banco-component', require('./components/bancos/DetailComponent.vue').default);
Vue.component('create-edit-banco-component', require('./components/bancos/CreateEditComponent').default);
Vue.component('table-banco-component', require('./components/bancos/TableComponent').default);

Vue.component('detail-cuenta-component', require('./components/cuentas/DetailComponent.vue').default);
Vue.component('create-edit-cuenta-component', require('./components/cuentas/CreateEditComponent').default);
Vue.component('table-cuenta-component', require('./components/cuentas/TableComponent').default);

Vue.component('detail-dependencia-component', require('./components/dependencias/DetailComponent.vue').default);
Vue.component('create-edit-dependencia-component', require('./components/dependencias/CreateEditComponent').default);
Vue.component('table-dependencia-component', require('./components/dependencias/TableComponent').default);

Vue.component('detail-formapago-component', require('./components/formapagos/DetailComponent.vue').default);
Vue.component('create-edit-formapago-component', require('./components/formapagos/CreateEditComponent').default);
Vue.component('table-formapago-component', require('./components/formapagos/TableComponent').default);

Vue.component('detail-incidencia-component', require('./components/incidencias/DetailComponent.vue').default);
Vue.component('create-edit-incidencia-component', require('./components/incidencias/CreateEditComponent').default);
Vue.component('table-incidencia-component', require('./components/incidencias/TableComponent').default);

Vue.component('detail-metodo-component', require('./components/metodos/DetailComponent.vue').default);
Vue.component('create-edit-metodo-component', require('./components/metodos/CreateEditComponent').default);
Vue.component('table-metodo-component', require('./components/metodos/TableComponent').default);

Vue.component('detail-status-component', require('./components/status/DetailComponent.vue').default);
Vue.component('create-edit-status-component', require('./components/status/CreateEditComponent').default);
Vue.component('table-status-component', require('./components/status/TableComponent').default);

Vue.component('detail-paquete-component', require('./components/paquetes/DetailComponent.vue').default);
Vue.component('create-edit-paquete-component', require('./components/paquetes/CreateEditComponent').default);
Vue.component('table-paquete-component', require('./components/paquetes/TableComponent').default);

Vue.component('detail-paquetesolicitud-component', require('./components/paquete_solicitud/DetailComponent.vue').default);
Vue.component('create-edit-paquetesolicitud-component', require('./components/paquete_solicitud/CreateEditComponent').default);
Vue.component('table-paquetesolicitud-component', require('./components/paquete_solicitud/TableComponent').default);

Vue.component('detail-anexo-component', require('./components/anexos/DetailComponent.vue').default);
Vue.component('create-edit-anexo-component', require('./components/anexos/CreateEditComponent').default);
Vue.component('table-anexo-component', require('./components/anexos/TableComponent').default);

Vue.component('detail-beneficiario-component', require('./components/beneficiarios/DetailComponent.vue').default);
Vue.component('create-edit-beneficiario-component', require('./components/beneficiarios/CreateEditComponent').default);
Vue.component('table-beneficiario-component', require('./components/beneficiarios/TableComponent').default);

Vue.component('detail-bolsa-component', require('./components/bolsas/DetailComponent.vue').default);
Vue.component('create-edit-bolsa-component', require('./components/bolsas/CreateEditComponent').default);
Vue.component('table-bolsa-component', require('./components/bolsas/TableComponent').default);

Vue.component('detail-compensacion-component', require('./components/compensaciones/DetailComponent.vue').default);
Vue.component('create-edit-compensacion-component', require('./components/compensaciones/CreateEditComponent').default);
Vue.component('table-compensacion-component', require('./components/compensaciones/TableComponent').default);

Vue.component('detail-compensacionorigen-component', require('./components/compensaciones_origen/DetailComponent.vue').default);
Vue.component('create-edit-compensacionorigen-component', require('./components/compensaciones_origen/CreateEditComponent').default);
Vue.component('table-compensacionorigen-component', require('./components/compensaciones_origen/TableComponent').default);

Vue.component('detail-factura-component', require('./components/facturas/DetailComponent.vue').default);
Vue.component('create-edit-factura-component', require('./components/facturas/CreateEditComponent').default);
Vue.component('table-factura-component', require('./components/facturas/TableComponent').default);

Vue.component('detail-provedor-component', require('./components/provedores/DetailComponent.vue').default);
Vue.component('create-edit-provedor-component', require('./components/provedores/CreateEditComponent').default);
Vue.component('table-provedor-component', require('./components/provedores/TableComponent').default);

Vue.component('detail-partida-component', require('./components/partidas/DetailComponent.vue').default);
Vue.component('create-edit-partida-component', require('./components/partidas/CreateEditComponent').default);
Vue.component('table-partida-component', require('./components/partidas/TableComponent').default);

Vue.component('detail-proyecto-component', require('./components/proyectos/DetailComponent.vue').default);
Vue.component('create-edit-proyecto-component', require('./components/proyectos/CreateEditComponent').default);
Vue.component('table-proyecto-component', require('./components/proyectos/TableComponent').default);

Vue.component('detail-movimiento-component', require('./components/movimientos/DetailComponent.vue').default);
Vue.component('create-edit-movimiento-component', require('./components/movimientos/CreateEditComponent').default);
Vue.component('table-movimiento-component', require('./components/movimientos/TableComponent').default);

Vue.component('detail-reintegro-component', require('./components/reintegros/DetailComponent.vue').default);
Vue.component('create-edit-reintegro-component', require('./components/reintegros/CreateEditComponent').default);
Vue.component('table-reintegro-component', require('./components/reintegros/TableComponent').default);

Vue.component('detail-complementopago-component', require('./components/complemento_pagos/DetailComponent.vue').default);
Vue.component('create-edit-complementopago-component', require('./components/complemento_pagos/CreateEditComponent').default);
Vue.component('table-complementopago-component', require('./components/complemento_pagos/TableComponent').default);

Vue.component('detail-facturaarchivo-component', require('./components/factura_archivos/DetailComponent.vue').default);
Vue.component('create-edit-facturaarchivo-component', require('./components/factura_archivos/CreateEditComponent').default);
Vue.component('table-facturaarchivo-component', require('./components/factura_archivos/TableComponent').default);

Vue.component('detail-complementopagoarchivo-component', require('./components/complemento_pago_archivos/DetailComponent.vue').default);
Vue.component('create-edit-complementopagoarchivo-component', require('./components/complemento_pago_archivos/CreateEditComponent').default);
Vue.component('table-complementopagoarchivo-component', require('./components/complemento_pago_archivos/TableComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

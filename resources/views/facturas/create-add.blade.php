@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-factura-component :factura="{{$factura}}" :solicitudes="{{$solicitudes}}"  :status="{{$status}}"  :forma_pagos="{{$forma_pagos}}" :create="{{$create}}" :metodos="{{$metodos}}" />
        </div>
    </div>
@endsection

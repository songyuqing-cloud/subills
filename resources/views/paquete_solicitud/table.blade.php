@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <table-paquetesolicitud-component :paquetes_solicitudes="{{$paquetes_solicitudes}}" />
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-proyecto-component :fondos="{{$fondos}}" :create="{{$create}}" :proyecto="{{$proyecto}}" />
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-incidencia-component v-bind:create="{{$create}}" :incidencia="{{$incidencia}}" />
        </div>
    </div>
@endsection
  
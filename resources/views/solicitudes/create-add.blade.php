@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-solicitud-component :beneficiarios="{{$beneficiarios}}"
                                             :incidencias="{{$incidencias}}"
                                             :fondos="{{$fondos}}"
                                             :proyectos="{{$proyectos}}"
                                             :status="{{$status}}"
                                             :users="{{$users}}"
                                             :create="{{$create}}"
                                             :solicitud="{{$solicitud}}" >
            </create-edit-solicitud-component>
        </div>
    </div>
@endsection

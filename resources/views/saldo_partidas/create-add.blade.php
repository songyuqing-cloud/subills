@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-saldopartidas-component :fondos="{{$fondos}}"
                                             :partidas="{{$partidas}}"
                                             :create="{{$create}}"
                                             :saldo_partida="{{$saldo_partida}}" >
            </create-edit-saldopartidas-component>
        </div>
    </div>
@endsection

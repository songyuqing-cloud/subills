@extends('layouts.app')

@section('navbar')
    @include('layouts.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <side-bar></side-bar>
        </div>
        <div class="col-md-8">
            <create-edit-facturaarchivo-component
                                             :solicitudes="{{$solicitudes}}"
                                             :create="{{$create}}"
                                             :factura_archivo="{{$factura_archivo}}" >
            </create-edit-facturaarchivo-component>
        </div>
    </div>
@endsection


@extends('layouts.app')

@section('content')
    <div class="row login-container d-flex justify-content-center align-content-center full-v" id="login-container">
        <div class=" d-none d-sm-none d-md-flex  col-sm-8 justify-content-center align-items-center has-background-grey full-v">
            <img src="{{ asset('img/udg_logo_white.svg') }}" width="30%">
        </div>
        <div class="col-md-4  d-flex justify-content-center align-items-center">
            <div class="">
                <div class="col-md-12">
                    <div class="contenedor-login-form">
                        <h3>CACE - Bienvenido</h3>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group ">
                                <label for="email" class="text-md-right text-white">Correo electrónico</label>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group ">
                                <label for="password" class="text-md-right text-white">Contraseña</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group ">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label text-caption" for="remember">
                                        Recuérdame
                                    </label>
                                </div>
                            </div>

                            <div class="form-group  d-flex justify-content-end ">
                                <button type="submit" class="btn btn-info form-control text-white">
                                    Entrar
                                </button>
                            </div>
                            <hr class="text-white">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
